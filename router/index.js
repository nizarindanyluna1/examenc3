import express from "express";
import json from 'body-parser';
export const router = express. Router();

router.get('/',(req,res)=>{

    const params = {

        num : req.query.num || '',
        nombre : req.query.nombre || '',
        domicilio : req.query.domicilio || '',
        nivel : req.query.nivel || '',
        pagoHora : req.query.pagoHora || '',
        horas : req.query.horas || '',
        numHijos : req.query.numHijos || ''

    }
    res.render('index',params);
})

router.post('/bono', (req, res) => {
    let nivel = req.body.nivel;
    let bonoHijos = req.body.numHijos;
    nivel = nivelFuncion(nivel);
    bonoHijos = numHijos(bonoHijos);

    const params = {
        pagoHora: req.body.pagoHora,
        horas: req.body.horas,
        nivel: nivel,
        bonoHijos: bonoHijos
    }
    res.render('bono', params);
});


function nivelFuncion(nivel){
    if(nivel === '1'){
        nivel = 1.3;
    }
    else if(nivel === '2'){
        nivel = 1.5;
    }
    else if(nivel === '3'){
        nivel = 2;
    }
    return nivel;
}

function numHijos(numHijos){
    if(numHijos === '1'){
        numHijos = 0.05;
    }
    else if(numHijos === '2'){
        numHijos = 0.10;
    }
    else if(numHijos === '3'){
        numHijos = 0.2;
    }else if(numHijos === '4'){
        numHijos = 0;
    }
    return numHijos;
}

export default { router }